# Example service

An example of a microservice running in a docker container. This service does very little. The files of interest are `Dockerfile` and `app/controllers/products_controller.rb`